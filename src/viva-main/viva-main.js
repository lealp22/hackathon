import { LitElement, html, css } from "lit-element";
import "../viva-form-add/viva-form-add.js";
import "../viva-form-init/viva-form-init.js"
import "../viva-send-api/viva-send-api.js";
import "../viva-api-get-user/viva-api-get-user.js";
import "../viva-form-person-info/viva-form-person-info.js";
import "../viva-form-reward-info/viva-form-reward-info.js";
import "../viva-api-post-request/viva-api-post-request.js";
import "../viva-show-error/viva-show-error.js";

class VivaMain extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    static styles = css`

      .form {
        background-color: #072146;
        border-radius: 20px;
        box-sizing: border-box;
        padding: 20px;
        width: 580px;        
      }

      .title {
        color: #eee;
        font-family: sans-serif;
        font-size: 36px;
        font-weight: 600;
        margin-top: 30px;
      }

      .subtitle {
        color: #2FCBCD;
        font-family: sans-serif;
        font-size: 16px;
        font-weight: 600;
        margin-top: 10px;
      }
    `;

    render() {
 
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
            <div class="form">  

                <div class="title">VIVA Awards 2021</title>
                <div class="subtitle">¡Apuntate y consigue tu premio!</div>

                <div class="row d-none" id="errorMessage">
                    <viva-show-error></viva-show-error>
                </div>

                <div class="row d-none" id="createUser">
                    <viva-form-add 
                        @viva-form-send="${this.vivaFormSendAdd}"
                        @viva-form-close="${this.vivaFormClose}" 
                        class="border rounded border-primary" id="vivaForm">
                    </viva-form-add>
                </div>

                <div class="row" id="getUser">
                    <viva-form-init 
                        @viva-form-init-send="${this.vivaFormInitSend}"
                        class="border rounded border-primary" id="vivaForm">
                    </viva-form-init>
                </div>          

                <div class="row d-none" id="infoUser">
                    <viva-form-person-info
                        @viva-suscription-request="${this.suscriptionRequest}"
                        @viva-form-person-info-close="${this.vivaInit}">
                    </viva-form-person-info>
                </div>

                <div class="row d-none" id="infoAward">
                    <viva-form-reward-info
                        @viva-form-reward-info-close="${this.vivaInit}">
                    </viva-form-reward-info>
                </div>
                            
                <viva-send-api
                    @viva-send-api-ok="${this.vivaSendApiOk}">
                </viva-send-api>

                <viva-api-get-user
                    @viva-api-get-user-ok="${this.showPerson}"
                    @viva-api-get-user-not-found="${this.showPersonNotFound}">
                </viva-api-get-user>

                <viva-api-post-request
                    @send-respond-request="${this.respondRequest}">
                </viva-api-post-request>

            </div>
        `;
    }

    // Se ejecuta al cambiar el valor de alguna de las propiedades
    updated(changedProperties) {
        console.log("updated");

    }

    showUserInfo() {
        console.log("showUserInfo");
        console.log("Mostramos información de la persona");
        this.shadowRoot.getElementById("createUser").classList.add("d-none");
        this.shadowRoot.getElementById("getUser").classList.add("d-none"); 
        this.shadowRoot.getElementById("infoUser").classList.remove("d-none"); 
        this.shadowRoot.getElementById("infoAward").classList.add("d-none");                      
    }    

    showGetUser() {
        console.log("showUserInfo");
        console.log("Mostramos información de la persona");
        this.shadowRoot.getElementById("createUser").classList.add("d-none");
        this.shadowRoot.getElementById("getUser").classList.remove("d-none"); 
        this.shadowRoot.getElementById("infoUser").classList.add("d-none"); 
        this.shadowRoot.getElementById("infoAward").classList.add("d-none");                      
    }     

    showCreateUser() {
        console.log("showCreateUser");
        console.log("Mostramos alta persona");
        this.shadowRoot.getElementById("createUser").classList.remove("d-none");
        this.shadowRoot.getElementById("getUser").classList.add("d-none"); 
        this.shadowRoot.getElementById("infoUser").classList.add("d-none"); 
        this.shadowRoot.getElementById("infoAward").classList.add("d-none");                      
    }     

    showInfoAward() {
        console.log("showInfoAward");
        console.log("Mostramos información premio");
        this.shadowRoot.getElementById("createUser").classList.add("d-none");
        this.shadowRoot.getElementById("getUser").classList.add("d-none"); 
        this.shadowRoot.getElementById("infoUser").classList.add("d-none"); 
        this.shadowRoot.getElementById("infoAward").classList.remove("d-none");          
    }

    showErrorMessage() {
        this.shadowRoot.getElementById("errorMessage").classList.remove("d-none");
    }

    hideErrorMessage() {
        this.shadowRoot.getElementById("errorMessage").classList.add("d-none");
    }

    // Gestiona el evento viva-api-get-user-ok de viva-api-get-user
    showPerson(e) {
        console.log("showPerson");
        console.log(e.detail.person);

        this.shadowRoot.querySelector("viva-form-person-info").person = e.detail.person;
    }

    showPersonNotFound(e) {
        console.log("showPersonNotFound");

        this.shadowRoot.querySelector("viva-show-error").message = "Usuario no encontrado. Desde este formulario puede darlo de alta";
        this.showErrorMessage();
        this.showCreateUser();
    }

    vivaSendApiOk(e) {
        console.log("vivaSendApiOk");
        this.shadowRoot.querySelector("viva-show-error").message = "Usuario creado correctamente";
        this.showErrorMessage();
        this.showGetUser();
    }

    suscriptionRequest(e) {
        console.log("suscriptionRequest");
        console.log(e.detail.userid);

        this.showInfoAward();
        this.shadowRoot.querySelector("viva-api-post-request").userid = e.detail.userid;
    }

    respondRequest(e) {
        console.log("respondRequest");
        console.log(e.detail.reward);

        this.shadowRoot.querySelector("viva-form-reward-info").reward = e.detail.reward;
    }

    vivaFormSendAdd(e) {
        console.log("vivaFormSendAdd");
        console.log("Se va almacenar una viva");

        console.log("La propiedad userid vale " + e.detail.person.userid);
        console.log("La propiedad name vale " + e.detail.person.name);
        console.log("La propiedad surname vale " + e.detail.person.surname);
        console.log("La propiedad age vale " + e.detail.person.age);

        let person = {};
        person.userid = e.detail.person.userid;
        person.name = e.detail.person.name;
        person.surname = e.detail.person.surname;
        person.age = e.detail.person.age;

        this.shadowRoot.querySelector("viva-send-api").person = person;

        console.log("Proceso terminado. Persona guardada o actualizada");
    }

    vivaFormInitSend(e) {
        console.log("vivaFormInitSend");
        console.log("Se va consultar un userid");

        console.log("La propiedad userid vale " + e.detail.userid);

        this.hideErrorMessage();
        this.showUserInfo();
        this.shadowRoot.querySelector("viva-api-get-user").userid = e.detail.userid;

        console.log("Proceso terminado. Persona guardada o actualizada");
    }

    vivaFormClose(e) {
        console.log("vivaFormCloseAdd");
        console.log("Se ha cerrado el formulario de viva");

        this.hideErrorMessage();
        this.showGetUser();
    }

    vivaInit(e) {
        console.log("vivaInit");

        this.showGetUser();
    }
}

customElements.define("viva-main", VivaMain);