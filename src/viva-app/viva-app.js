import { LitElement, html } from "lit-element";
import "../viva-main/viva-main.js";
import "../viva-footer/viva-footer.js";

class VivaApp extends LitElement {

    static get properties() {
        return {
         }
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
            <div class="row">
                <viva-main 
                    class="col-12">
                </viva-main>
            </div>
            <viva-footer></viva-footer>
        `;
    }

    // Se ejecuta al actualizar alguna de las propiedades
    updated(changedProperties) {
        console.log("updated en viva-app");
        console.log(changedProperties);

    }
}

customElements.define("viva-app", VivaApp);