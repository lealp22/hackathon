import { LitElement, html, css } from "lit-element";

class VivaFormAdd extends LitElement {

    static get properties() {
        return {
            person: {type: Object}
        }
    }

    constructor() {
        super();

        this.resetFormData();
    }

    static styles = css`

        .form {
            height: 800px;
        }

        .input-container {
            height: 50px;
            position: relative;
            width: 100%;
        }
    
        .ic1 {
            margin-top: 40px;
          }
          
        .ic2 {
            margin-top: 30px;
        }

        .input {
            background-color: rgb(218 218 218);
            border-radius: 12px;
            border: 0;
            box-sizing: border-box;
            color: rgb(51 51 51);
            font-size: 18px;
            height: 100%;
            outline: 0;
            padding: 4px 20px 0;
            width: 100%;
          }

        .cut {
            background-color: #072146;
            border-radius: 10px;
            height: 20px;
            left: 20px;
            position: absolute;
            top: -20px;
            transform: translateY(0);
            transition: transform 200ms;
            width: 106px;
          }

          .cut-short {
            width: 50px;
          }
          
          .input:focus ~ .cut,
          .input:not(:placeholder-shown) ~ .cut {
            transform: translateY(8px);
          }

          .placeholder {
            color: rgb(80 81 82);
            font-size: 22px;
            font-family: sans-serif;
            left: 20px;
            line-height: 14px;
            pointer-events: none;
            position: absolute;
            transform-origin: 0 50%;
            transition: transform 200ms, color 200ms;
            top: 20px;
          }
          
          .input:focus ~ .placeholder,
          .input:not(:placeholder-shown) ~ .placeholder {
            transform: translateY(-30px) translateX(10px) scale(0.75);
          }
          
          .input:not(:placeholder-shown) ~ .placeholder {
            color: #808097;
          }
          
          .input:focus ~ .placeholder {
            color: #65657b;
          }
          
          button {
            background-color: #018484;
            border-radius: 12px;
            border: 0;
            box-sizing: border-box;
            color: #eee;
            cursor: pointer;
            font-size: 16px;
            height: 50px;
            margin-top: 38px;
            text-align: center;
            width: 100%;
            z-index: 1;
            position: relative;
            overflow: hidden;
          }

          button:hover {
            color: white;
          }

          button::after {
            content: "";
            background: #2BCCCD ;
            position: absolute;
            z-index: -1;
            padding: 0.85em 0.75em;
            display: block;
          }
          button::after {
            transition: all 0.35s;
          }
          button[class^="slide"]:hover::after {
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            transition: all 0.35s;
          }
          button.slide_from_left::after {
            top: 0;
            bottom: 0;
            left: -100%;
            right: 100%;
          }
    `;

    render() {
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

        <div>
                <div class="input-container ic1">
                    <input @input="${this.updateUserId}" 
                        id="userid"
                        class="input"
                        placeholder=""
                        .value="${this.person.userid}" 
                        type="text" />
                        <div class="cut"></div>
                        <label for="userid" class="placeholder">ID usuario</label>
                </div>
                <div class="input-container ic2">
                    <input @input="${this.updateName}" 
                        id="name"
                        class="input"
                        placeholder=""
                        .value="${this.person.name}" 
                        type="text" />
                        <div class="cut"></div>
                        <label for="name" class="placeholder">Nombre</label>
                </div>
                <div class="input-container ic2">
                    <input @input="${this.updateSurname}" 
                        id="surname"
                        class="input"
                        placeholder=""
                        .value="${this.person.surname}" 
                        type="text" />
                        <div class="cut"></div>
                        <label for="surname" class="placeholder">Apellido</label>
                </div>
                <div class="input-container ic2">
                    <input @input="${this.updateAge}" 
                        id="age"
                        class="input"
                        placeholder=""
                        .value="${this.person.age}" 
                        type="text" />
                        <div class="cut"></div>
                        <label for="age" class="placeholder">Edad</label>
                </div>

                <button @click="${this.sendPerson}" class="slide_from_left"><strong>Enviar</strong></button>
                <button @click="${this.goBack}" class="slide_from_left"><strong>Atrás</strong></button>
        </div>
        `;
    }

    updateUserId(e) {
        console.log("updateUserID");
        console.log("Actualizamos el valor de la propiedad userid con valor " + e.target.value);

        this.person.userid = e.target.value;
    }

    updateName(e) {
        console.log("updateName");
        console.log("Actualizamos el valor de la propiedad name con valor " + e.target.value);

        this.person.name = e.target.value;
    }

    updateSurname(e) {
        console.log("updateSurname");
        console.log("Actualizamos el valor de la propiedad surname con valor " + e.target.value);

        this.person.surname = e.target.value;
    }

    updateAge(e) {
        console.log("updateAge");
        console.log("Actualizamos el valor de la propiedad age con valor " + e.target.value);

        this.person.age = e.target.value;
    }

    goBack(e) {
        console.log("goBack");
        console.log("Se ha cerrado el formulario de persona");
        e.preventDefault();
        this.resetFormData();

        this.dispatchEvent(new CustomEvent("viva-form-close", {}));
    }

    sendPerson(e) {
        console.log("sendPerson");
        console.log("Se va a enviar una persona");
        e.preventDefault();

        console.log("La propiedad userid vale " + this.person.userid);
        console.log("La propiedad name vale " + this.person.name);
        console.log("La propiedad surname vale " + this.person.surname);
        console.log("La propiedad age vale " + this.person.age);

        this.dispatchEvent(new CustomEvent("viva-form-send", {
                detail: {
                    person: {
                        userid: this.person.userid,
                        name: this.person.name,
                        surname: this.person.surname,
                        age: this.person.age
                     }
                 }
            })
        )
        this.resetFormData();
    }

    resetFormData() {
        console.log("resetFormData");

        this.person = {};
        this.person.userid = "";
        this.person.name = "";
        this.person.surname = "";
        this.person.age = "";
    }
}

customElements.define("viva-form-add", VivaFormAdd);